import { BUBBLEROLLS } from "../config.js";
CONFIG.bubblerolls = {};

Handlebars.registerHelper('dice', function (faces) {
    switch (faces) {
        case 4:
            return "/modules/bubblerolls/assets/d4.png";
        case 6:
            return "/modules/bubblerolls/assets/d6.png";
        case 8:
            return "/modules/bubblerolls/assets/d8.png";
        case 10:
            return "/modules/bubblerolls/assets/d10.png";
        case 12:
            return "/modules/bubblerolls/assets/d12.png";
        case 20:
            return "/modules/bubblerolls/assets/d20.png";
        case 100:
            return "/modules/bubblerolls/assets/d10.png";
    }
    return "none"
});

Handlebars.registerHelper('truncate', function(inputstring, start, end) {
    var truncated = inputstring.substring( start, end);
    return new Handlebars.SafeString(truncated)
 });

Hooks.once('init', async function () {
    // Localize template labels
    BUBBLEROLLS.templates = Object.entries(BUBBLEROLLS.templates).reduce((obj, e) => {
        obj[e[0]] = game.i18n.localize(e[1]);
        return obj;
    }, {});

    game.settings.register("bubblerolls", "BubbleRollTemplate", {
        name: game.i18n.localize("BUBBLEROLLS.SettingsName"),
        hint: game.i18n.localize("BUBBLEROLLS.SettingsHint"),
        scope: "client",
        config: true,
        choices: BUBBLEROLLS.templates,
        default: "/modules/bubblerolls/templates/bubble-details.html",
        type: String,
        onChange: value => {
            CONFIG.bubblerolls.template = value;
        }
    })
    CONFIG.bubblerolls.template = game.settings.get("bubblerolls", "BubbleRollTemplate");
});

Hooks.on("renderChatMessage", async function (msg) {
    // Don't display old chat messages at refresh
    if ((Date.now() - Date.parse(msg.data.timestamp)) > 500) {
        return;
    }
    if (Object.getOwnPropertyNames(msg.data.speaker).length > 0 && msg.isRoll && msg.isRollVisible) {
        canvas.tokens.placeables.forEach((tok) => {
            if (tok.id == msg.data.speaker.token) {
                renderTemplate(CONFIG.bubblerolls.template, msg).then(html => {
                    canvas.hud.bubbles.say(tok, html, false);
                })
            }
        });
    }
});
