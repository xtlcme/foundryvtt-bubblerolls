export const BUBBLEROLLS = {};

BUBBLEROLLS.templates = {
    "/modules/bubblerolls/templates/bubble-details.html": "BUBBLEROLLS.RollDetailsTemplate",
    "/modules/bubblerolls/templates/bubble-flavor.html": "BUBBLEROLLS.RollFlavorTemplate",
    "/modules/bubblerolls/templates/bubble-result.html": "BUBBLEROLLS.RollResultTemplate"
};